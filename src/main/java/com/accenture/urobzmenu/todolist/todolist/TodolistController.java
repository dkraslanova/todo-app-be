package com.accenture.urobzmenu.todolist.todolist;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItem;
import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItemMapper;
import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItemRepository;

@RestController
@CrossOrigin(origins = "http://localhost:3000")

public class TodolistController {

	private DbToDoItemRepository repository;

	private ToDoItemValidatorExternalConfiguration configuration;

	public TodolistController(DbToDoItemRepository repository, ToDoItemValidatorExternalConfiguration configuration) {
		System.out.println("todolist controller constructor");
		this.repository = repository;
		this.configuration = configuration;
	}

	@RequestMapping(value = "/todos", method = RequestMethod.POST)
	public void addTodoItem(@RequestBody TodoItem input) {
		System.out.println("New TODO item received: " + input);

		TodoItemValidator.validate(input, configuration);
		TodoItemValidator.validate(repository, configuration);
		TodoItemValidator.validateDueDate(input);

		DbToDoItem dbToDoItem = DbToDoItemMapper.map(input);
		repository.save(dbToDoItem);
	}

	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public List<TodoItem> fetch() {
		Iterable<DbToDoItem> dbToDoItems = repository.findAllSortedBydueDate();

		List<TodoItem> response = new LinkedList<>();

		for (DbToDoItem dbToDoItem : dbToDoItems) {
			TodoItem toDoItem = DbToDoItemMapper.map(dbToDoItem);
			response.add(toDoItem);
		}

		return response;
	}

	@RequestMapping(value = "/todos/delete/all", method = RequestMethod.DELETE)
	public void removeAll() {
		repository.deleteAll();

	}

	@RequestMapping(value = "/todos/{id}", method = RequestMethod.DELETE)
	public void removeTodoItem(@PathVariable String id) {
		repository.deleteById(id);
	}

	@RequestMapping(value = "/todos/{id}", method = RequestMethod.PUT)
	public void updateTodoItem(@RequestBody TodoItem request, @PathVariable String id) {
		Optional<DbToDoItem> optionalDbToDoItem = repository.findById(id);

		if (optionalDbToDoItem.isPresent()) {
			DbToDoItem dbToDoItem = optionalDbToDoItem.get();

			dbToDoItem.setCompleted(request.isCompleted());
			if (request.getName() != null) {
				dbToDoItem.setName(request.getName());
			}
			if (request.getDescription() != null) {
				dbToDoItem.setDescription(request.getDescription());
			}
			if (request.getDueDate() != null) {
				dbToDoItem.setLocalDate(request.getDueDate());
			}
			
			dbToDoItem.setFavorite(request.getFavorite());
			

			repository.save(dbToDoItem);
		}
	}

}
