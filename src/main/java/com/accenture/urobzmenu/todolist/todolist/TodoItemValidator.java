package com.accenture.urobzmenu.todolist.todolist;

import java.time.LocalDate;
import java.util.List;

import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItemRepository;

public class TodoItemValidator {

	public static void validate(TodoItem todoItem, ToDoItemValidatorExternalConfiguration configuration) {
		if (todoItem.getName().length() > configuration.getNameSize()) {
			throw new RuntimeException("Toto nie je validny todoItem" + "Expected : " + configuration.getNameSize());
		}
//		if (todoItem.getName().length() ==0) {
////			throw new RuntimeException ("ziadny input");
//		}
	}

	public static void validate(DbToDoItemRepository repository, ToDoItemValidatorExternalConfiguration configuration) {
		if (repository.count() >= configuration.getListSize()) {
			throw new RuntimeException("Privela zaznamov v liste!" + "Expected : " + configuration.getListSize());
		}
	} 

	public static void validate(TodoItem newToDoItem, List<TodoItem> list) {
		for (TodoItem doItem : list) {
			if (doItem.getName().equals(newToDoItem.getName())) {
				throw new RuntimeException("Nazov je duplicitny");
			}
		}
	}
	public static void validateDueDate (TodoItem newToDoItem) {
		if (newToDoItem.getDueDate().isBefore(LocalDate.now())) {
			throw new RuntimeException ("Nespravny datum");
		}
	}
}
