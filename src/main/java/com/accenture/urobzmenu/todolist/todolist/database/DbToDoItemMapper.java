package com.accenture.urobzmenu.todolist.todolist.database;

import com.accenture.urobzmenu.todolist.todolist.TodoItem;

public class DbToDoItemMapper {
	
	public static DbToDoItem map (TodoItem input) {
		DbToDoItem dbToDoItem = new DbToDoItem();
		dbToDoItem.setName(input.getName());
		dbToDoItem.setDescription(input.getDescription());
		dbToDoItem.setCompleted(input.isCompleted());
		dbToDoItem.setLocalDate(input.getDueDate());
		dbToDoItem.setFavorite(input.getFavorite());
		
		return dbToDoItem;
	}
	
	public static TodoItem map(DbToDoItem dbToDoItem) {
	
	TodoItem toDoItem = new TodoItem();
	toDoItem.setId(dbToDoItem.getId());
	toDoItem.setName(dbToDoItem.getName());
	toDoItem.setDescription(dbToDoItem.getDescription());
	toDoItem.setCompleted(dbToDoItem.isCompleted());
	toDoItem.setLocalDate(dbToDoItem.getLocalDate());
	toDoItem.setFavorite(dbToDoItem.getFavorite());

	return toDoItem;
	}

}
