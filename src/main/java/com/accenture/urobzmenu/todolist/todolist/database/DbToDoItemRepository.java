package com.accenture.urobzmenu.todolist.todolist.database;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DbToDoItemRepository extends CrudRepository<DbToDoItem, String> {
	
//	@Query("select todo from TODO_ITEM todo order by")
//	
//	Iterable<DbToDoItem> findAllSortedByName();
//	
//	Iterable<DbToDoItem> findAllByOrderByNameAsc();
//	
//	Iterable<DbToDoItem> findAllByOrderByCompletedAsc();
//	
//	Iterable<DbToDoItem> findAllByCompletedOrderByNameAsc(boolean completed);
//	
//	Iterable<DbToDoItem> findAllByOrderByCompletedAscNameDesc();
	
	@Query("select todo from TODO_ITEM todo order by todo.dueDate asc ")
	Iterable<DbToDoItem> findAllSortedBydueDate();
	
//	@Query()

}
