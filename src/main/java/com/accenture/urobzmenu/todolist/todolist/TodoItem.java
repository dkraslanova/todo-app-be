package com.accenture.urobzmenu.todolist.todolist;

import java.time.LocalDate;

public class TodoItem {
	
	private String id;
	private String name;
	private String description;
	private boolean completed;
	private String dateAndTime;
	private LocalDate dueDate;
	private boolean favorite;

	@Override
	public String toString() {
		return "(ToDoItem) name: "+ name +", description: " + description + ", completed: "+completed+""+ "dueDate: " + dueDate+"";
	}

	public String getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setLocalDate(LocalDate localDate) {
		this.dueDate = localDate;
	}

	public boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

}
