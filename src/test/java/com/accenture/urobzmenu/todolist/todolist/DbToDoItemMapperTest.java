package com.accenture.urobzmenu.todolist.todolist;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItem;
import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItemMapper;

class DbToDoItemMapperTest {

	@Test
	void testMapToDoItem() {
		DbToDoItem item = new DbToDoItem();
		item.setId("ID");
		TodoItem result = DbToDoItemMapper.map(item);

		assertNotNull(result);
		assertEquals("ID", result.getId());
	}

	@Test
	void testMapDbToDoItem() {
		TodoItem item = new TodoItem();

		DbToDoItem result = DbToDoItemMapper.map(item);

		assertNotNull(result);
		assertEquals("ID", result.getId());
	}

}
