package com.accenture.urobzmenu.todolist.todolist;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItem;
import com.accenture.urobzmenu.todolist.todolist.database.DbToDoItemRepository;

class ToDoListControllerTest {

	private TodolistController controller;
	private DbToDoItemRepository repository;
	private ToDoItemValidatorExternalConfiguration configuration;

	@BeforeEach
	void setUp() throws Exception {
		repository = mock(DbToDoItemRepository.class);
		configuration = mock(ToDoItemValidatorExternalConfiguration.class);
		controller = new TodolistController(repository, configuration);
	}

	@Test
	void testAddTodoItem() {
//		prepare
		doReturn(5).when(configuration).getNameSize();
		doReturn(5).when(configuration).getListSize();
		TodoItem item = new TodoItem();
		item.setName("Name");
		item.setCompleted(false);
		item.setDescription("description");
		item.setLocalDate(LocalDate.of(2021, 6, 30));
//		act
		controller.addTodoItem(item);
//		verify
		verify(repository, times(1)).save(any());
	}
	@Test
	void testAddTodoItem_NullPointerException() {
//		prepare
		TodoItem item = new TodoItem();
		
//		act and verify
		assertThrows(NullPointerException.class, () -> {
			this.controller.addTodoItem(item);
		});
	}
	@Test
	void testFetch_1ItemList() {
//		prepare
		List<DbToDoItem> dbToDoItems = new ArrayList<DbToDoItem>();
		DbToDoItem item = new DbToDoItem();
		item.setCompleted(false);
		item.setId("DDD");
		item.setName("Name");
		dbToDoItems.add(item);
		doReturn(dbToDoItems).when(this.repository).findAllSortedBydueDate();
//		act
		List<TodoItem> result = this.controller.fetch();
//		verify
		assertNotNull (result);
		assertEquals(1, result.size());
		
	}
	@Test 
	void testRemoveById() {
//		prepare
//		act
		controller.removeTodoItem("1");
//		verify
		verify(repository, times(1)).deleteById(any());
	}
	void testRemoveAll() {
//		prepare
//		act
		controller.removeAll();
//		verify
		verify(repository, times(1)).deleteAll();
	}
	
}
